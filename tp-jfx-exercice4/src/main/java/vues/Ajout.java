package vues;

import controleur.Controleur;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import modeleLudotheque.CategorieJeu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class Ajout extends Vue implements VueInteractive, EcouteurOrdre {
    private Controleur controleur;

    @FXML
    VBox mainVbox;

    @FXML
    TextField nomJeu;
    @FXML
    ComboBox<CategorieJeu> categorie;
    @FXML
    TextArea resume;

    public Parent getTop() {
        return mainVbox;
    }


    public static Ajout creerVue(GestionnaireVue gestionnaireVue)  {
        FXMLLoader fxmlLoader = new FXMLLoader(Ajout.class.getResource("ajout.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("Probleme de construction de vue Formulaire");
        }

        Ajout vue = fxmlLoader.getController();
        gestionnaireVue.ajouterVueInteractive(vue);
        gestionnaireVue.ajouterEcouteurOrdre(vue);
        vue.setScene(new Scene(vue.getTop()));
        return vue;
    }

    public void gotoAjout(ActionEvent actionEvent) {
        controleur.gotoAjout();
    }

    public  void chargerGenres(){
        this.categorie.setItems(FXCollections.observableList(new ArrayList<>(controleur.getCategories())));
    }
    @Override
    public void setControleur(Controleur controleur) {

        this.controleur = controleur;

    }

    public void creerJeu(ActionEvent actionEvent) {
        controleur.creerJeu(nomJeu.getText(), categorie.getSelectionModel().getSelectedItem(), resume.getText());
    }

    public void viderChamps() {
        nomJeu.setText("");
        categorie.getSelectionModel().selectFirst();
        resume.setText("");
    }

    public void afficherErreur(String titre, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR,message);
        alert.setTitle(titre);
        alert.showAndWait();

    }

    public void afficherConfirmation(String message) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Enregistrement du jeu réussi", ButtonType.OK);
        alert.setContentText(message);
        alert.showAndWait();

    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this,
                TypeOrdre.ERREUR_CATEGORIE_INEXISTANT,
                TypeOrdre.ERREUR_CATEGORIE_VIDE,
                TypeOrdre.ERREUR_JEU_EXISTANT,
                TypeOrdre.ERREUR_RESUME_VIDE,
                TypeOrdre.ERREUR_NOMJEU_VIDE,
                TypeOrdre.ERREUR_CHAMPS_VIDE,
                TypeOrdre.SHOW_AJOUT);
    }

    @Override
    public void traiter(TypeOrdre e) {
        switch (e){
            case ERREUR_CHAMPS_VIDE:{
                this.afficherErreur("Erreur saisie","Les champs ne peuvent être vides !");
                break;
            }
            case ERREUR_CATEGORIE_VIDE:{
                this.afficherErreur("Erreur de categorie", "La categorie n'a pas été sélectionner");
                break;
            }
            case ERREUR_NOMJEU_VIDE:{
                this.afficherErreur("Erreur de nom du jeu", "Le nom du jeu n'a pas été rentrer");
                break;
            }
            case ERREUR_RESUME_VIDE:{
                this.afficherErreur("Erreur de resume", "Le resume n'a pas été rentrer");
                break;
            }
            case ERREUR_CATEGORIE_INEXISTANT:{
                this.afficherErreur("Erreur de categorie", "Categorie inexistant !");
                this.viderChamps();
                break;
            }
            case ERREUR_JEU_EXISTANT:{
                this.afficherErreur("Erreur de jeu", "Le nom du jeu existe déjà !");
                this.viderChamps();
                break;
            }
            case SHOW_AJOUT:{
                Collection<CategorieJeu> genres = this.controleur.getCategories();
                this.categorie.setItems(FXCollections.observableArrayList(genres));
                this.categorie.getSelectionModel().selectFirst();
                this.viderChamps();
                break;
            }
        }
    }
}
