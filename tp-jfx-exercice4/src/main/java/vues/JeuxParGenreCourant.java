package vues;

import controleur.Controleur;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import modeleLudotheque.Jeu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class JeuxParGenreCourant extends Vue implements VueInteractive, EcouteurOrdre {
    private Controleur controleur;

    @FXML
    VBox mainVbox;

    @FXML
    ListView<Jeu> lesJeux;

    public Parent getTop() {
        return mainVbox;
    }

    public static JeuxParGenreCourant creerVue(GestionnaireVue gestionnaireVue) {
        FXMLLoader fxmlLoader = new FXMLLoader(JeuxParGenreCourant.class.getResource("jeuParGenreCourant.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("Probleme de construction de vue Formulaire");
        }
        JeuxParGenreCourant vue = fxmlLoader.getController();

        gestionnaireVue.ajouterVueInteractive(vue);
        gestionnaireVue.ajouterEcouteurOrdre(vue);
        vue.setScene(new Scene(vue.getTop()));
        vue.costumizeListView();
        return vue;

    }
    public void gotoAjout(ActionEvent actionEvent) {
        controleur.gotoAjout();
    }


    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    private void costumizeListView() {
        this.lesJeux.setCellFactory(param -> new ListCell<Jeu>() {
            @Override
            protected void updateItem(Jeu item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null ) {
                    setText("No body");
                } else {
                    setText(item.getNomJeu() + " ("+item.getCategorieJeu()+") "+item.getResume());
                }
            }
        });
    }


    @Override
    public void show() {
        Collection<Jeu> jeux = controleur.getJeuxParGenreCourant();
        this.lesJeux.setItems(FXCollections.observableList(new ArrayList<>(jeux)));
        super.show();
    }

    private Controleur getControleur() {
        return this.controleur;
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this,TypeOrdre.SHOW_JEUX_PAR_GENRE_COURANT);
    }

    @Override
    public void traiter(TypeOrdre e) {
        switch (e){
            case SHOW_JEUX_PAR_GENRE_COURANT:{
                Collection<Jeu> jeux = controleur.getJeuxParGenreCourant();
                this.lesJeux.setItems(FXCollections.observableList(new ArrayList<>(jeux)));
            }
        }
    }
}
