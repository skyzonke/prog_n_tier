package vues;


import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.stage.Stage;

public class GestionnaireVueImpl extends GestionnaireVue {
    private Ajout ajout;
    private JeuxParGenreCourant jeuxParGenreCourant;



    public GestionnaireVueImpl(Stage stage) {
        super(stage);
        this.ajout = Ajout.creerVue(this);
        this.jeuxParGenreCourant = JeuxParGenreCourant.creerVue(this);
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        super.setAbonnement(g);
        g.abonnement(this,
                TypeOrdre.SHOW_AJOUT,
                TypeOrdre.SHOW_JEUX_PAR_GENRE_COURANT);

    }


    @Override
    public void traiter(TypeOrdre e) {
        switch (e) {
            case SHOW_AJOUT:{
                this.getStage().setScene(ajout.getScene());
                break;
            }
            case SHOW_JEUX_PAR_GENRE_COURANT:{
                this.getStage().setScene(jeuxParGenreCourant.getScene());
            }

        }
        this.getStage().show();

    }
}
