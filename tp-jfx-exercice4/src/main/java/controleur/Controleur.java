package controleur;

import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import facadeLudotheque.*;
import modeleLudotheque.CategorieJeu;
import modeleLudotheque.Jeu;
import vues.Ajout;
import vues.GestionnaireVue;

import java.util.*;

public class Controleur implements LanceurOrdre {
    private Ajout ajout;
    private CategorieJeu categorieCourant;
    private FacadeLudotheque facadeLudotheque;
    private Map<TypeOrdre,Collection<EcouteurOrdre>> abonnes;

    public Controleur(GestionnaireVue gestionnaireVue) {
        this.abonnes = new HashMap<>();
        Arrays.stream(TypeOrdre.values())
                .forEach(
                        typeOrdre ->
                                this.abonnes.put(typeOrdre,new ArrayList<>()));
        this.facadeLudotheque = new FacadeLudothequeImpl();
        gestionnaireVue.setControleur(this);
        gestionnaireVue.setAbonnement(this);
    }

    public void showAjout(){
        ajout.show();
    }


    public void run() {
        this.fireOrdre(TypeOrdre.SHOW_AJOUT);
    }

    public void creerJeu(String nomJeu, CategorieJeu categorie, String resume)  {
        if (Objects.isNull(nomJeu)&&Objects.isNull(categorie)&&Objects.isNull(resume)&&nomJeu.equals("")&&resume.equals("")){
            this.fireOrdre(TypeOrdre.ERREUR_CHAMPS_VIDE);

        }
        else {
            try {
                facadeLudotheque.ajoutJeu(nomJeu, categorie.name(), resume);
                this.categorieCourant = categorie;
                this.gotoLesJeuxParGenres();
            } catch (CategorieNotFoundException e) {
                this.fireOrdre(TypeOrdre.ERREUR_CATEGORIE_INEXISTANT);
            } catch (InformationManquanteException e) {
                if (Objects.isNull(nomJeu) || nomJeu.equals("")){
                    this.fireOrdre(TypeOrdre.ERREUR_NOMJEU_VIDE);

                } else if (Objects.isNull(resume) || resume.equals("")) {
                    this.fireOrdre(TypeOrdre.ERREUR_RESUME_VIDE);
                }
                else if (Objects.isNull(categorie)) {
                    this.fireOrdre(TypeOrdre.ERREUR_CATEGORIE_VIDE);
                }
            } catch (JeuDejaExistant e) {
                this.fireOrdre(TypeOrdre.ERREUR_JEU_EXISTANT);
            }


        }
}

    public Collection<Jeu> getJeuxParGenreCourant() {
        try {
            return this.facadeLudotheque.getJeuxDuneCategorie(this.categorieCourant.name());
        } catch (CategorieNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    public void gotoLesJeuxParGenres() {
        this.fireOrdre(TypeOrdre.SHOW_JEUX_PAR_GENRE_COURANT);
    }
    public void gotoAjout() {
        this.fireOrdre(TypeOrdre.SHOW_AJOUT);
    }

    public Collection<CategorieJeu> getCategories(){
        return this.facadeLudotheque.getAllCategorie();
    }

    @Override
    public void abonnement(EcouteurOrdre ecouteurOrdre, TypeOrdre... types) {
        Arrays.stream(types).forEach(typeOrdre ->
        {this.abonnes.get(typeOrdre).add(ecouteurOrdre);});
    }

    @Override
    public void fireOrdre(TypeOrdre e) {
        this.abonnes.get(e)
                .stream()
                .forEach(abonne ->
                        abonne.traiter(e));
    }
}
