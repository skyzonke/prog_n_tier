<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Bastien
  Date: 26/01/2023
  Time: 12:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Calculatrice</title>
    <jsp:useBean id="password" scope="request" type="java.lang.String"/>
    <jsp:useBean id="pseudo" scope="request" type="java.lang.String"/>
</head>
<body>

<h1>${pseudo}</h1>

<div>
    <s:text name="page.connectedSentence"/>
</div>

<a href="/accueil"><s:text name="page.backToMenu"/></a>

</body>
</html>
