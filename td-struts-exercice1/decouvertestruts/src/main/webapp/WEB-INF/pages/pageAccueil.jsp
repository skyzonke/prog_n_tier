<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Bastien
  Date: 26/01/2023
  Time: 11:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="page.titre"/></title>
</head>
<body>

<h1><s:text name="page.menuName"/></h1>

<ul>
    <li><a href="/connexion"><s:text name="page.connexionName"/></a></li>
    <li><a href="/calculatriceStatique"><s:text name="page.CalculatriceStaticName"/></a></li>
    <li><a href="/calculatriceDynamique"><s:text name="page.CalculatriceDynaName"/></a></li>
</ul>

</body>
</html>

