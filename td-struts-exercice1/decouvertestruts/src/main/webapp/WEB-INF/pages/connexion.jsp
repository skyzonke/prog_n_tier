<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Bastien
  Date: 26/01/2023
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="page.titre"/></title>
</head>
<body>
<form action="infoSaisies.action" method="post">

    <s:fielderror fieldName="pseudo"/><label for="id:pseudo"><s:text name="page.pseudoName"/></label><input id="id:pseudo" name="pseudo" type="text">
    <s:fielderror fieldName="password"/><label for="id:password"><s:text name="page.passwordName"/></label><input id="id:password" name="password" type="password">
    <input type="submit" name="Connexion!">
</form>

<br>

</body>
</html>
