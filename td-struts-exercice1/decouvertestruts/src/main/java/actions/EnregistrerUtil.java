package actions;

import com.opensymphony.xwork2.ActionSupport;

public class EnregistrerUtil extends ActionSupport {

    private String pseudo;
    private String password;

    public String getPseudo() {
        return pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws Exception {
        return LOGIN;
    }

    @Override
    public void validate() {
        if("".equals(pseudo)) {
            addFieldError("pseudo", getText("pseudo.required"));
        }
        else if(pseudo.length()<3){
            addFieldError("pseudo", "pseudo must be longer than 3 character");
        }
        if("".equals(password)) {
            addFieldError("password", getText("password.required"));
        }
        else if(password.length()<3){
            addFieldError("password", "password must be longer than 3 character");
        }
    }
}


