package vues;

import controleur.Controleur;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import modeleCreaFilm.Film;

import java.io.IOException;
import java.util.Collection;

public class Ajout implements VueInteractive {

    @FXML
    public BorderPane borderPane;
    @FXML
    public Button retour;
    @FXML
    public TextField titre;
    @FXML
    public TextField genre;
    @FXML
    public TextField realisateur;

    public static Ajout creerVue(Controleur controleur, Stage stage) {
        FXMLLoader fxmlLoader = new FXMLLoader(Ajout.class.
                getResource("/Ajout.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException("Probleme de construction de vue Formulaire");
        }
        Ajout vue = fxmlLoader.getController();
        vue.setControleur(controleur);
        vue.setStage(stage);
        vue.setScene(new Scene(vue.getTop()));
        return vue;
    }

    private Controleur controleur;

    private Stage stage;

    private Scene scene;

    public Parent getTop() {
        return borderPane;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    public Controleur getControleur() {
        return controleur;
    }

    public void show() {
        this.getStage().setScene(this.getScene());
        this.getStage().show();
    }

    public void afficherErreur(String erreurDeGenre, String s) {
        Alert alert = new Alert(Alert.AlertType.ERROR,erreurDeGenre, ButtonType.OK);
        alert.setContentText(s);
        alert.showAndWait();
    }

    public void viderChamps() {
        this.titre.setText("");
        this.genre.setText("");
        this.realisateur.setText("");
    }

    public void gotoAccueil(ActionEvent actionEvent) {
        this.getControleur().gotoMenu();
    }

    public void enregistrerFilm(ActionEvent actionEvent) {
        this.controleur.creerFilm(this.titre.getText(),this.genre.getText(),this.realisateur.getText());
    }
}
