package vues;

import controleur.Controleur;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class Menu implements VueInteractive {
    private static BorderPane borderPanePrincipal;
    private Scene scene;
    private Controleur controleur;
    private static Stage stage;

    public static Menu creerVue(Controleur controleur, Stage stage) {
        borderPanePrincipal = new BorderPane();
        Text title = new Text("Les films");
        Button buttonConsulter = new Button("Consulter");
        buttonConsulter.setOnAction(actionEvent -> {
            Menu menu = new Menu();
            menu.setControleur(controleur);
            menu.goToConsulter(actionEvent);
        });
        Button buttonAjouter = new Button("Ajouter");
        buttonAjouter.setOnAction(actionEvent -> {
            System.out.println("ajout handle");
            Menu menu = new Menu();
            menu.setControleur(controleur);
            menu.goToAjout(actionEvent);
        });
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(400,600);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setAlignment(Pos.CENTER);
        gridPane.add(buttonConsulter,1,1);
        gridPane.add(buttonAjouter,2,1);
        title.setTextAlignment(TextAlignment.CENTER);
        borderPanePrincipal.setTop(title);
        borderPanePrincipal.setCenter(gridPane);
        Scene scene = new Scene(borderPanePrincipal);
        Menu vue = new Menu();
        vue.setControleur(controleur);
        vue.setScene(scene);
        vue.setStage(stage);


        return vue;

    }

    public Parent getTop() {
        return borderPanePrincipal;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        Menu.stage = stage;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    public Controleur getControleur() {
        return controleur;
    }

    public void show() {
        this.getStage().setScene(this.getScene());
        this.getStage().show();
    }

    public void goToAjout(ActionEvent actionEvent){
        this.getControleur().gotoAjout();
    }

    public void goToConsulter(ActionEvent actionEvent){
        this.getControleur().gotoConsulter();
    }

}
