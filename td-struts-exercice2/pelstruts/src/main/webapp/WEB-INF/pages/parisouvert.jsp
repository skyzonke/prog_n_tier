<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="page.titre"/></title>
</head>
<body>

<h1><s:property value="#session.user.login" /></h1>
<s:fielderror fieldName="error"/>

<ul>
    <li><s:text name="parisOuverts.sentence1"/><s:property value=" parisOuverts.size()"/> </li>
    <li><s:text name="parisOuverts.sentence2"/>
        <ul>
            <s:iterator value="parisOuverts">
                <li> sport : <s:property value="Sport" /> - <s:property value="Equipe1" /> vs <s:property value="Equipe2" /> - <a href="/parier?idMatch=<s:property value="IdMatch" />"><s:text name="parisOuverts.bet"/></a>;
            </s:iterator>
        </ul>
</ul>
<a href="<s:text name="link.menu"/>"><s:text name="page.backToMenu"/></a>

</body>
</html>
