<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="page.titre"/></title>
</head>
<body>

<h1><s:property value="#session.user.login" /></h1>
<s:fielderror fieldName="error"/>

<ul>
    <li><s:text name="mesParis.sentence1"/> <s:property value="mesParis.size()" /> </li>
    <li><s:text name="mesParis.sentence2"/>
        <ul>
            <s:iterator value="mesParis" var="x">
            <s:set var="match" value="#x.getMatch()"/>
            <li> <s:text name="mesParis.sentence3"/> <s:property value="#match.getSport()" /> - <s:property value="#match.getEquipe1()" /> VS <s:property value="#match.getEquipe2()" />  - <s:text name="mesParis.sentence4"/> <s:property value="#match.getQuand()" />. <s:text name="mesParis.sentence5"/> <s:property value="#x.getMontant()" /> <s:text name="mesParis.sentence6"/> <s:property value="#x.getVainqueur()" /> <a href="/annuler?idPari=<s:property value="#x.getIdPari()" />"><s:text name="mesParis.cancel"/></a>;
                </s:iterator>
        </ul>
</ul>
<a href="<s:text name="link.menu"/>"><s:text name="page.backToMenu"/></a>

</body>
</html>

