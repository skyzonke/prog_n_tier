<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="page.titre"/></title>
</head>
<body>

<h1><s:text name="page.menuName"/></h1>

<ul>
    <li><a href="parisouverts"><s:text name="page.openBets"/></a></li>
    <li><a href="mesparis"><s:text name="page.bets"/></a></li>
    <li><a href="deconnexion"><s:text name="page.disconnect"/></a></li>
    <s:if test= "#session.user.admin">
        <li><a href="${pageContext.request.contextPath}/"><s:text name="page.admin"/></a></li>
    </s:if>
</ul>




</body>
</html>
