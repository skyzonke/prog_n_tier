<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="page.titre"/></title>
</head>
<body>

<h1><s:property value="#session.user.login" /></h1>

<s:set var="match" value="#pari.getMatch()"/>
<div>
    <s:text name="confirmationPari.sentence1"/> <s:property value="pari.getMontant()" /> <s:text name="confirmationPari.sentence2"/> <s:property value="pari.getVainqueur()" /> <s:text name="confirmationPari.sentence3"/> <s:property value="match.getEquipe1()" /> vs <s:property value="match.getEquipe2()" /> <s:text name="confirmationPari.sentence4"/> <s:property value="match.getQuand()" />
</div>

<a href="<s:text name="link.menu"/>"><s:text name="page.backToMenu"/></a>

</body>
</html>

