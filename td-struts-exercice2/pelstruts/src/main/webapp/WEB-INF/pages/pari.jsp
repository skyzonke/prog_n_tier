<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><s:text name="page.titre"/></title>
</head>
<body>

<h1><s:property value="#session.user.login" /></h1>
<s:fielderror fieldName="error"/>

<div>
    <s:text name="pari.sentence1"/> <s:property value="match.getEquipe1()" /> vs <s:property value="match.getEquipe2()" /> <s:text name="pari.sentence2"/> <s:property value="match.getQuand()" />
</div>
<form action="<s:text name="pari.postLink"/>" method="post">
    <div>

        <label>
            <s:text name="pari.sentence3"/>
            <select name="verdict">
                <option value="<s:property value="match.getEquipe1()" />"><s:property value="match.getEquipe1()" /></option>
                <option value="<s:property value="match.getEquipe2()" />"><s:property value="match.getEquipe2()" /></option>
                <option value="nul"><s:text name="pari.sentence4"/></option>
            </select>
        </label>
        <label for="montant"><s:text name="pari.sentence5"/></label><input id="montant" name="montant" type="text"><input type="submit" name="Parier !">
        <input type="hidden" name="idMatch" value="<s:property value="match.getIdMatch()" />">
    </div>

</form>
<a href="<s:text name="link.menu"/>"><s:text name="page.backToMenu"/></a>

</body>
</html>
