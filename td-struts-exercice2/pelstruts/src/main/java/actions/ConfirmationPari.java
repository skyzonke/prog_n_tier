package actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.inject.Inject;
import facade.FacadeParis;
import facade.exceptions.MatchClosException;
import facade.exceptions.MontantNegatifOuNulException;
import facade.exceptions.ResultatImpossibleException;
import modele.Match;
import modele.Pari;
import modele.Utilisateur;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class ConfirmationPari extends ActionSupport implements SessionAware {
    private Map<String, Object> session;

    public Map<String, Object> getSession() {
        return session;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    private long idMatch;

    public long getidMatch() {
        return idMatch;
    }

    public void setidMatch(long idMatch) {
        this.idMatch = idMatch;
    }

    private FacadeParis facade;

    public FacadeParis getFacade() {
        return facade;
    }

    @Inject("facade")
    public void setFacade(FacadeParis facade) {
        this.facade = facade;
    }

    private Pari pari;

    public Pari getPari() {
        return pari;
    }

    public void setPari(Pari pari) {
        this.pari = pari;
    }

    private String verdict;

    public long getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(long idMatch) {
        this.idMatch = idMatch;
    }

    public String getVerdict() {
        return verdict;
    }

    public void setVerdict(String verdict) {
        this.verdict = verdict;
    }

    private int montant;

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

    private Match match;

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    @Override
    public String execute() {
        match = facade.getMatch(idMatch);
        return SUCCESS;
    }

    @Override
    public void validate() {
        try {
            Utilisateur user = (Utilisateur) session.get("user");
            long idPari = facade.parier(user.getLogin(),idMatch,verdict,montant);
            pari = facade.getPari(idPari);
        } catch (MatchClosException e) {
            addFieldError("error",getText("confirmationPari.MatchClosException"));
        } catch (ResultatImpossibleException e) {
            addFieldError("error",getText("confirmationPari.ResultatImpossibleException"));
        } catch (MontantNegatifOuNulException e) {
            addFieldError("error",getText("confirmationPari.MontantNegatifOuNulException"));
        }
    }
}
