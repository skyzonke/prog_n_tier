package actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.inject.Inject;
import facade.FacadeParis;
import facade.exceptions.InformationsSaisiesIncoherentesException;
import facade.exceptions.UtilisateurDejaConnecteException;
import modele.Utilisateur;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class Connexion extends ActionSupport implements SessionAware {

    private String pseudo;
    private String password;
    private FacadeParis facade;
    private Utilisateur user;

    private Map<String, Object> session;

    @Override
    public void setSession(Map<String, Object> session){
        this.session=session;
    }


    public Utilisateur getUser() {
        return user;
    }

    public void setUser(Utilisateur user) {
        this.user = user;
    }

    public FacadeParis getFacade() {
        return facade;
    }

    @Inject("facade")
    public void setFacade(FacadeParis facade) {
        this.facade = facade;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getPassword() {
        return password;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws Exception {
        session.put("user",user);
        return LOGIN;
    }


}


