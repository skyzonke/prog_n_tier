package actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.inject.Inject;
import facade.FacadeParis;
import facade.exceptions.OperationNonAuthoriseeException;
import modele.Pari;
import modele.Utilisateur;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class Annuler extends ActionSupport implements SessionAware {
    private Map<String, Object> session;

    public Map<String, Object> getSession() {
        return session;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    private long idPari;

    public long getIdPari() {
        return idPari;
    }

    public void setIdPari(long idPari) {
        this.idPari = idPari;
    }

    private FacadeParis facade;

    public FacadeParis getFacade() {
        return facade;
    }

    @Inject("facade")
    public void setFacade(FacadeParis facade) {
        this.facade = facade;
    }

    private Pari pari;

    public Pari getPari() {
        return pari;
    }

    public void setPari(Pari pari) {
        this.pari = pari;
    }

    @Override
    public String execute() {
        return SUCCESS;
    }

    @Override
    public void validate() {
        try {
            Utilisateur user = (Utilisateur) session.get("user");
            facade.annulerPari(user.getLogin(),idPari);
            pari = facade.getPari(idPari);
        } catch (OperationNonAuthoriseeException e) {
            addFieldError("error",getText("annuler.OperationNonAuthoriseeException"));
        }
    }
}
