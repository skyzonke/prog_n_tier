package actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.inject.Inject;
import facade.FacadeParis;
import modele.Match;
import modele.Pari;
import modele.Utilisateur;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public class Parier extends ActionSupport {
    private long idMatch;

    public long getidMatch() {
        return idMatch;
    }

    public void setidMatch(long idMatch) {
        this.idMatch = idMatch;
    }

    private FacadeParis facade;

    public FacadeParis getFacade() {
        return facade;
    }

    @Inject("facade")
    public void setFacade(FacadeParis facade) {
        this.facade = facade;
    }

    private Match match;

    public long getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(long idMatch) {
        this.idMatch = idMatch;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    @Override
    public String execute() throws Exception {
        match = facade.getMatch(idMatch);
        return SUCCESS;
    }
}
