package actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.inject.Inject;
import facade.FacadeParis;
import modele.Pari;
import modele.Utilisateur;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Collection;
import java.util.Map;

public class MesParis extends ActionSupport implements SessionAware {
    private FacadeParis facade;
    private Utilisateur user;

    private Collection<Pari> mesParis;

    private Map<String, Object> session;

    public Map<String, Object> getSession() {
        return session;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public Collection<Pari> getMesParis() {
        return mesParis;
    }

    public void setMesParis(Collection<Pari> mesParis) {
        this.mesParis = mesParis;
    }

    public Utilisateur getUser() {
        return user;
    }

    public void setUser(Utilisateur user) {
        this.user = user;
    }

    public FacadeParis getFacade() {
        return facade;
    }

    @Inject("facade")
    public void setFacade(FacadeParis facade) {
        this.facade = facade;
    }


    @Override
    public String execute() throws Exception {
        user = (Utilisateur) session.get("user");
        this.mesParis = facade.getMesParis(user.getLogin());
        return SUCCESS;
    }
}
