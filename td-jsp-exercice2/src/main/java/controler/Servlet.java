package controler;

import facade.FacadeParisStaticImpl;
import facade.exceptions.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import modele.Match;
import modele.Pari;
import modele.Utilisateur;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Servlet extends HttpServlet {
    public static final String MENU_JSP = "/WEB-INF/pages/menu.jsp";
    public static final String PARISOUVERTS_JSP = "/WEB-INF/pages/parisouvert.jsp";
    public static final String MESPARIS_JSP = "/WEB-INF/pages/mesparis.jsp";
    private static String HOME_JSP="/WEB-INF/pages/home.jsp";
    private static String ANNULER_JSP="/WEB-INF/pages/annuler.jsp";

    private static String CONFIRMATION_PARI_JSP="/WEB-INF/pages/confirmationPari.jsp";

    private static String PARIER_JSP="/WEB-INF/pages/pari.jsp";

    private static String CLE_CONNEXION="connexion";
    private static String CLE_DECONNEXION="deconnexion";
    private static String CLE_PARISOUVERTS="parisouverts";
    private static String CLE_MESPARIS="mesparis";
    private static String CLE_MENU="menu";
    private static String CLE_ANNULER="annuler";
    private static String CLE_PARIER="parier";


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        FacadeParisStaticImpl facade = (FacadeParisStaticImpl)
                request.getServletContext().getAttribute("facade");

        if (Objects.isNull(facade)) {
            facade = new FacadeParisStaticImpl();
            request.getServletContext().setAttribute("facade",facade);
        }

        String destination = HOME_JSP;
        request.setAttribute("user", (Utilisateur) request.getSession().getAttribute("user"));
        String[] url = request.getRequestURI().split("/");
        // 1. On récupère la clé de navigation à partir de l'URI
        String cleNavigation = url[url.length-1];

        if (CLE_DECONNEXION.equals(cleNavigation)) {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
            facade.deconnexion(user.getLogin());
            destination=HOME_JSP;
        }

        if (CLE_PARISOUVERTS.equals(cleNavigation)) {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
            Collection<Match> matchs = facade.getParisOuverts(user.getLogin());
            request.setAttribute("matchsouverts", matchs);
            destination=PARISOUVERTS_JSP;
        }

        if (CLE_MESPARIS.equals(cleNavigation)) {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
            Collection<Pari> paris = facade.getMesParis(user.getLogin());
            request.setAttribute("mesparis", paris);
            destination=MESPARIS_JSP;
        }

        if (CLE_MENU.equals(cleNavigation)) {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
            if (user != null){
                destination=MENU_JSP;
            }
            else {
                destination = HOME_JSP;
            }
        }

        if (CLE_ANNULER.equals(cleNavigation)) {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
            Long idPari =  Long.parseLong(request.getParameter("idPari"));
            Pari pari = facade.getPari(idPari);
            request.setAttribute("pari",pari);
            Collection<Pari> paris = facade.getMesParis(user.getLogin());
            request.setAttribute("mesparis", paris);
            try {
                facade.annulerPari(user.getLogin(),idPari);
                destination = ANNULER_JSP;
            } catch (OperationNonAuthoriseeException e) {
                request.setAttribute("messageErreur","le paris ne peux pas êtres annuler");
                destination = MESPARIS_JSP;
            }
        }

        if (CLE_PARIER.equals(cleNavigation)) {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
            Long idMatch =  Long.parseLong(request.getParameter("idMatch"));
            Match match = facade.getMatch(idMatch);
            request.setAttribute("match",match);
            destination= PARIER_JSP;
        }

        request.getServletContext()
                .getRequestDispatcher(destination)
                .forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String destination = HOME_JSP;
        String[] url = request.getRequestURI().split("/");
        // 1. On récupère la clé de navigation à partir de l'URI
        String cleNavigation = url[url.length-1];


        FacadeParisStaticImpl facade = (FacadeParisStaticImpl)
                request.getServletContext().getAttribute("facade");

        if (Objects.isNull(facade)) {
            facade = new FacadeParisStaticImpl();
            request.getServletContext().setAttribute("facade",facade);
        }


        if (CLE_CONNEXION.equals(cleNavigation)) {
            String pseudo = request.getParameter("pseudo");
            String password = request.getParameter("password");

            try {
                Utilisateur user = facade.connexion(pseudo,password);
                request.getSession().setAttribute("user",user);
                destination=MENU_JSP;

            } catch (UtilisateurDejaConnecteException e) {
                request.setAttribute("messageErreur","Utilisateur déjà connecté !");
                destination=HOME_JSP;
            } catch (InformationsSaisiesIncoherentesException e) {
                request.setAttribute("messageErreur","Couple pseudo/password incorrect !");
                destination=HOME_JSP;
            }catch (IdentifiantManquantException e){
                request.setAttribute("messageErreur","le pseudo est obligatoire et de taille 2 minimum,le mot de passe est obligatoire et de taille 2 minimum ");
                destination=HOME_JSP;
            }

        }

        if (CLE_PARIER.equals(cleNavigation)) {
            Utilisateur user = (Utilisateur) request.getSession().getAttribute("user");
            Long idMatch =  Long.parseLong(request.getParameter("idMatch"));
            Match match = facade.getMatch(idMatch);
            String vainqueur = request.getParameter("verdict");
            try {
                double montant = Double.parseDouble(request.getParameter("montant"));
                facade.parier(user.getLogin(),idMatch,vainqueur,montant);
                request.setAttribute("match",match);
                destination= CONFIRMATION_PARI_JSP;
            } catch (MatchClosException e) {
                request.setAttribute("messageErreur","Le match a commencé il n'est plus possible de parier");
                destination=PARISOUVERTS_JSP;
            } catch (ResultatImpossibleException e) {
                request.setAttribute("messageErreur","Le résultat que vous avez rentrer est impossible");
                request.setAttribute("match",match);
                destination=PARIER_JSP;
            } catch (MontantNegatifOuNulException e) {
                request.setAttribute("messageErreur","Vous devez saisir un montant positif pour votre mise");
                request.setAttribute("match",match);
                destination=PARIER_JSP;
            }catch (NumberFormatException e){
                request.setAttribute("messageErreur","Le montant doit être remplie");
                request.setAttribute("match",match);
                destination=PARIER_JSP;
            }
        }


        request.getServletContext().getRequestDispatcher(destination).forward(request,response);
    }
}
