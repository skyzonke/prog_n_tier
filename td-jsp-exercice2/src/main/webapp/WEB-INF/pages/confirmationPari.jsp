<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Paris en ligne</title>
    <jsp:useBean id="match" type="modele.Match" scope="request"/>
</head>
<body>

<h1>${user.login}</h1>

<div>
    vous avez parié ${montant} sur le résultat ${verdict} pour le match : ${match.getEquipe1()} vs ${match.getEquipe2()} le ${match.getQuand()}
</div>

<a href="/pel/menu">Retour au menu</a>

</body>
</html>
