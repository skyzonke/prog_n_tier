<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Paris en ligne</title>
    <jsp:useBean id="matchsouverts" type="java.util.Collection<modele.Match>" scope="request"/>
</head>
<body>

<h1>${user.login}</h1>

<ul>
    <li> Nombre de matchs sur lesquelles il est possible de parier : ${matchsouverts.size()}</li>
    <li> Liste des matchs ouvert au paris :
        <ul>
            <c:forEach items="${matchsouverts}" var="x">
                <li> sport : ${x.getSport()} - ${x.getEquipe1()} vs ${x.getEquipe2()} - <a href="/pel/parier?idMatch=${x.getIdMatch()}">parier</a>;
            </c:forEach>
        </ul>
</ul>
<a href="/pel/menu">Retour au menu</a>

</body>
</html>
