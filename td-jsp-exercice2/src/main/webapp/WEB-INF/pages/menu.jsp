<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h1>Menu</h1>

<ul>
    <li><a href="/pel/parisouverts">Listes des paris ouverts</a></li>
    <li><a href="/pel/mesparis">Affichage des paris</a></li>
    <li><a href="/pel/deconnexion">Déconnexion</a></li>
</ul>
<c:if test = "${user.isAdmin()}" >
               <li><a href="/pel/mesparis">Page admin</a></li>
</c:if>

</body>
</html>
