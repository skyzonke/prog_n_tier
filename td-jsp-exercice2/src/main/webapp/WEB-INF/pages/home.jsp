<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Gestion de collections d'items</title>
</head>
<body>

<form action="/pel/connexion" method="post">

    <label for="pseudo">Pseudo</label> <input id="pseudo" name="pseudo" type="text">
    <label for="password">Password</label><input id="password" name="password" type="password">
    <input type="submit" name="Connexion!">
</form>

<br>

<span style="color: red; ">
    ${messageErreur}
</span>
</body>
</html>