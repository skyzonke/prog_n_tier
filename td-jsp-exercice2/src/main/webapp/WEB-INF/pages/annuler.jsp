<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Paris en ligne</title>
    <jsp:useBean id="pari" type="modele.Pari" scope="request"/>
</head>
<body>

<h1>${user.login}</h1>

<div> La mise de ${pari.getMontant()} euros sur le résultat ${pari.getVainqueur()} pour le match ${pari.getMatch().getEquipe1()} vs ${pari.getMatch().getEquipe2()} le ${pari.getMatch().getQuand()} a bien été annulée !

<a href="/pel/menu">Retour au menu</a>

</body>
</html>
