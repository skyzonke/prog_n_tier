<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Paris en ligne</title>
    <jsp:useBean id="mesparis" type="java.util.Collection<modele.Pari>" scope="request"/>
</head>
<body>

<h1>${user.login}</h1>
<span style="color: red; ">
    ${messageErreur}
</span>

<ul>
    <li> Nombre de paris en cours : "${mesparis.size()}"</li>
    <li> Liste des paris en cours :
        <ul>
            <c:forEach items="${mesparis}" var="x">
                <li> sport : ${x.getMatch().getSport()} - ${x.getMatch().getEquipe1()} vs ${x.getMatch().getEquipe2()} - le ${x.getMatch().getQuand()}. Mise de ${x.getMontant()} sur ${x.getVainqueur()} <a href="/pel/annuler?idPari=${x.getIdPari()}">annuler</a>;
            </c:forEach>
        </ul>
</ul>

<a href="/pel/menu">Retour au menu</a>

</body>
</html>
