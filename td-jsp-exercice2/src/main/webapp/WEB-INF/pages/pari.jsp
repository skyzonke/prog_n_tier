<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  Created by IntelliJ IDEA.
  User: o22004836@campus.univ-orleans.fr
  Date: 12/01/2023
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Paris en ligne</title>
    <jsp:useBean id="match" type="modele.Match" scope="request"/>
</head>
<body>

<h1>${user.login}</h1>
<span style="color: red; ">
    ${messageErreur}
</span>

<div>
    Vous voulez parier sur le match : ${match.getEquipe1()} vs ${match.getEquipe2()} le ${match.getQuand()}
</div>
<form action="/pel/parier" method="post">
    <div>
        Verdict du match<select name="verdict">
            <option value="${match.getEquipe1()}">${match.getEquipe1()}</option>
            <option value="${match.getEquipe2()}">${match.getEquipe2()}</option>
            <option value="nul">nul</option>
        </select><label for="montant">Montant</label><input id="montant" name="montant" type="text"><input type="submit" name="Parier !">
        <input type="hidden" name="idMatch" value="${match.getIdMatch()}">
    </div>


<a href="/pel/menu">Retour au menu</a>

</body>
</html>
