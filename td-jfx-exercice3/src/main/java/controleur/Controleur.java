package controleur;

import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import facadeCreaFilm.FacadeScreen;
import facadeCreaFilm.FacadeScreenImpl;
import facadeCreaFilm.GenreNotFoundException;
import facadeCreaFilm.NomFilmDejaExistantException;
import javafx.stage.Stage;
import modeleCreaFilm.Film;
import modeleCreaFilm.GenreFilm;
import vues.Ajout;
import vues.GestionnaireVue;
import vues.Menu;
import vues.TousLesFilms;

import java.util.*;

public class Controleur implements LanceurOrdre {
    private Menu menu;
    private TousLesFilms tousLesFilms;
    private Ajout ajout;
    private GenreFilm genreCourant;
    private FacadeScreen facadeScreen;
    private Map<TypeOrdre,Collection<EcouteurOrdre>> abonnes;

    public Controleur(GestionnaireVue gestionnaireVue) {
        this.abonnes = new HashMap<>();
        Arrays.stream(TypeOrdre.values())
                .forEach(
                        typeOrdre ->
                                this.abonnes.put(typeOrdre,new ArrayList<>()));
        this.facadeScreen = new FacadeScreenImpl();
        gestionnaireVue.setControleur(this);
        gestionnaireVue.setAbonnement(this);
    }

    private void showMenu() {
        menu.show();
    }
    public void showFilms(){
        tousLesFilms.show();
    }
    public void showAjout(){
        ajout.show();
    }



    public void run() {
        this.fireOrdre(TypeOrdre.SHOW_ACCUEIL);
    }


    public void gotoConsulter() {

        this.fireOrdre(TypeOrdre.SHOW_TOUS_LES_FILMS);
    }

    public void gotoMenu() {
        this.fireOrdre(TypeOrdre.SHOW_ACCUEIL);
    }

    public void creerFilm(String titre, GenreFilm genre, String realisateur)  {
        if (Objects.isNull(titre)||Objects.isNull(genre)||Objects.isNull(realisateur)||titre.equals("")||realisateur.equals("")){
            this.fireOrdre(TypeOrdre.ERREUR_CHAMPS_VIDE);
        }
        else {
            try {
                facadeScreen.creerFilm(titre, realisateur, genre);
                this.fireOrdre(TypeOrdre.DATA_NOUVEAU_FILM);
                this.gotoMenu();

            } catch (GenreNotFoundException e) {
                this.fireOrdre(TypeOrdre.ERREUR_GENRE_INEXISTANT);
            } catch (NomFilmDejaExistantException e) {
                this.fireOrdre(TypeOrdre.ERREUR_FILM_EXISTANT);
            }
        }
}

    public void gotoAjout() {
        this.fireOrdre(TypeOrdre.SHOW_AJOUT);
    }

    public Collection<GenreFilm> getGenres() {
        return this.facadeScreen.getAllGenres();
    }

    public Collection<Film> getLesFilms() {
        return  facadeScreen.getAllFilms();
    }

    @Override
    public void abonnement(EcouteurOrdre ecouteurOrdre, TypeOrdre... types) {
        Arrays.stream(types).forEach(typeOrdre ->
        {this.abonnes.get(typeOrdre).add(ecouteurOrdre);});
    }

    @Override
    public void fireOrdre(TypeOrdre e) {
        this.abonnes.get(e)
                .stream()
                .forEach(abonne ->
                        abonne.traiter(e));
    }

    public void gotoRechercher() {
        this.fireOrdre(TypeOrdre.SHOW_RECHERCHER);
    }

    public void rechercherParGenre(GenreFilm genreFilm){
        this.genreCourant = genreFilm;
        this.fireOrdre(TypeOrdre.DATA_RECHERCHE_GENRE);
    }

    public Collection<Film> getFilmsByGenresCourant() {
        try {
            return this.facadeScreen.getFilmsDuGenre(this.genreCourant.name());
        } catch (GenreNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
