package vues;


import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.stage.Stage;

public class GestionnaireVueImpl extends GestionnaireVue {
    private Ajout ajout;
    private Menu menu;
    private TousLesFilms tousLesFilms;
    private RechercheParGenre rechercheParGenre;
    private FilmParGenreCourant filmParGenreCourant;



    public GestionnaireVueImpl(Stage stage) {
        super(stage);
        this.menu = Menu.creerVue(this);
        this.ajout = Ajout.creerVue(this);
        this.tousLesFilms = TousLesFilms.creerVue(this);
        this.rechercheParGenre = RechercheParGenre.creerVue(this);
        this.filmParGenreCourant = FilmParGenreCourant.creerVue(this);
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        super.setAbonnement(g);
        g.abonnement(this,
                TypeOrdre.SHOW_AJOUT,
                TypeOrdre.SHOW_ACCUEIL,
                TypeOrdre.SHOW_TOUS_LES_FILMS,
                TypeOrdre.SHOW_RECHERCHER,
                TypeOrdre.DATA_RECHERCHE_GENRE);

    }


    @Override
    public void traiter(TypeOrdre e) {
        switch (e) {
            case SHOW_AJOUT:{
                this.getStage().setScene(ajout.getScene());
                break;
            }

            case SHOW_ACCUEIL: {
                this.getStage().setScene(menu.getScene());
                break;
            }

            case SHOW_TOUS_LES_FILMS: {
                this.getStage().setScene(tousLesFilms.getScene());
                break;
            }
            case SHOW_RECHERCHER:{
                this.getStage().setScene(rechercheParGenre.getScene());
                break;
            }
            case DATA_RECHERCHE_GENRE:{
                this.getStage().setScene(filmParGenreCourant.getScene());
                break;
            }

        }
        this.getStage().show();

    }
}
