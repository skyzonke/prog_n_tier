package vues;

import controleur.Controleur;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import modeleCreaFilm.Film;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class FilmParGenreCourant extends Vue implements VueInteractive, EcouteurOrdre {
    @FXML
    TableColumn<Film,String> titre;
    @FXML
    TableColumn<Film,String> real;
    private Controleur controleur;

    @FXML
    VBox mainVbox;

    @FXML
    TableView<Film> lesFilms;

    public Parent getTop() {
        return mainVbox;
    }

    public static FilmParGenreCourant creerVue(GestionnaireVue gestionnaireVue) {
        FXMLLoader fxmlLoader = new FXMLLoader(FilmParGenreCourant.class.getResource("filmParGenreCourant.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("Probleme de construction de vue Formulaire");
        }
        FilmParGenreCourant vue = fxmlLoader.getController();

        gestionnaireVue.ajouterVueInteractive(vue);
        gestionnaireVue.ajouterEcouteurOrdre(vue);
        vue.setScene(new Scene(vue.getTop()));
        return vue;

    }
    public void gotomenu(ActionEvent actionEvent) {
        controleur.gotoMenu();
    }


    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }



    @Override
    public void show() {
        Collection<Film> films = controleur.getLesFilms();
        this.lesFilms.setItems(FXCollections.observableList(new ArrayList<>(films)));
        super.show();
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this,TypeOrdre.DATA_RECHERCHE_GENRE);
    }

    @Override
    public void traiter(TypeOrdre e) {
        switch (e){
            case DATA_RECHERCHE_GENRE:{
                Collection<Film> filmCollection = this.getControleur().getFilmsByGenresCourant();
                titre.setCellValueFactory(cellData -> cellData.getValue().titreProperty());
                real.setCellValueFactory(cellData -> cellData.getValue().realisateurProperty());
                lesFilms.setItems(FXCollections.observableArrayList(filmCollection));
                break;
            }
        }
    }

    private Controleur getControleur() {
        return this.controleur;
    }
}
