package vues;

import controleur.Controleur;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import facadeCreaFilm.GenreNotFoundException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import modeleCreaFilm.Film;
import modeleCreaFilm.GenreFilm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class RechercheParGenre extends Vue implements VueInteractive, EcouteurOrdre {
    private Controleur controleur;

    @FXML
    VBox mainVbox;

    @FXML
    ComboBox<GenreFilm> genre;

    public Parent getTop() {
        return mainVbox;
    }

    public static RechercheParGenre creerVue(GestionnaireVue gestionnaireVue) {
        FXMLLoader fxmlLoader = new FXMLLoader(RechercheParGenre.class.getResource("selectionGenre.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("Probleme de construction de vue Formulaire");
        }
        RechercheParGenre vue = fxmlLoader.getController();

        gestionnaireVue.ajouterVueInteractive(vue);
        gestionnaireVue.ajouterEcouteurOrdre(vue);
        vue.setScene(new Scene(vue.getTop()));
        return vue;

    }
    public void gotomenu(ActionEvent actionEvent) {
        controleur.gotoMenu();
    }


    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    @Override
    public void show() {

        super.show();
    }

    private Controleur getControleur() {
        return this.controleur;
    }

    public void rechercher(ActionEvent actionEvent) throws GenreNotFoundException {
        this.controleur.rechercherParGenre(this.genre.getValue());
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this,TypeOrdre.SHOW_RECHERCHER);
    }

    @Override
    public void traiter(TypeOrdre e) {
        switch (e){
            case SHOW_RECHERCHER:{
                Collection<GenreFilm> genres = this.controleur.getGenres();
                this.genre.setItems(FXCollections.observableArrayList(genres));
                this.genre.getSelectionModel().selectFirst();
                break;
            }
        }
    }
}
