package vues;

import controleur.Controleur;
import controleur.ordres.EcouteurOrdre;
import controleur.ordres.LanceurOrdre;
import controleur.ordres.TypeOrdre;
import facadeCreaFilm.GenreNotFoundException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import modeleCreaFilm.Film;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class TousLesFilms extends Vue implements VueInteractive, EcouteurOrdre {
    private Controleur controleur;

    @FXML
    VBox mainVbox;

    @FXML
    ListView<Film> lesFilms;

    public Parent getTop() {
        return mainVbox;
    }

    public static TousLesFilms creerVue(GestionnaireVue gestionnaireVue) {
        FXMLLoader fxmlLoader = new FXMLLoader(TousLesFilms.class.getResource("tousLesFilms.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            System.out.println("Probleme de construction de vue Formulaire");
        }
        TousLesFilms vue = fxmlLoader.getController();

        gestionnaireVue.ajouterVueInteractive(vue);
        gestionnaireVue.ajouterEcouteurOrdre(vue);
        vue.setScene(new Scene(vue.getTop()));
        vue.costumizeListView();
        return vue;

    }
    public void gotomenu(ActionEvent actionEvent) {
        controleur.gotoMenu();
    }


    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    private void costumizeListView() {
        this.lesFilms.setCellFactory(param -> new ListCell<Film>() {
            @Override
            protected void updateItem(Film item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null ) {
                    setText("No body");
                } else {
                    setText(item.getTitre() + " ("+item.getRealisateur()+", "+item.getGenre()+")");
                }
            }
        });
    }


    @Override
    public void show() {
        Collection<Film> films = controleur.getLesFilms();
        this.lesFilms.setItems(FXCollections.observableList(new ArrayList<>(films)));
        super.show();
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        g.abonnement(this,TypeOrdre.DATA_NOUVEAU_FILM,TypeOrdre.SHOW_TOUS_LES_FILMS,TypeOrdre.DATA_RECHERCHE_GENRE);
    }

    @Override
    public void traiter(TypeOrdre e) {
        switch (e){
            case DATA_NOUVEAU_FILM:
            case SHOW_TOUS_LES_FILMS: {
                Collection<Film> filmCollection = this.getControleur().getLesFilms();
                lesFilms.setItems(FXCollections.observableArrayList(filmCollection));
                break;
            }
            case DATA_RECHERCHE_GENRE:{
                Collection<Film> filmCollection = this.getControleur().getFilmsByGenresCourant();
                lesFilms.setItems(FXCollections.observableArrayList(filmCollection));
                break;
            }
        }
    }

    private Controleur getControleur() {
        return this.controleur;
    }
}
