package modeleCreaFilm;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Film {

    private int id;
    private StringProperty titre;
    private StringProperty realisateur;
    private GenreFilm genre;



    public static int ID =0;

    public Film(String titre, String realisateur,GenreFilm genre) {
        this.titre=new SimpleStringProperty(titre);
        this.realisateur=new SimpleStringProperty(realisateur);
        this.genre=genre;
        this.id = ID;
        ID++;
    }

    public String getTitre() {
        return titre.get();
    }

    public StringProperty titreProperty() {
        return titre;
    }

    public GenreFilm getGenre() {
        return genre;
    }

    public String getRealisateur() {
        return realisateur.get();
    }
    public StringProperty realisateurProperty() {
        return realisateur;
    }

    public int getId() {
        return id;
    }



}
